<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HourlyGeneration extends Model
{
    //
    public function getTimeAttribute()
    {
		return str_pad($this->month, 2, '0', STR_PAD_LEFT) . str_pad($this->day, 2, '0', STR_PAD_LEFT) . str_pad($this->hour, 2, '0', STR_PAD_LEFT);
    }
}
