<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Solar\Solar;
use Exception;

class CalculateApiController extends Controller
{
    private $request;

    //
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    //
    public function generation(Request $request)
    {
        try {
            //$params = Solar::testParams();
            $params = $this->request->all();
            $solar = new Solar($params);
            $solar->computerGenerationTable();

            return response()->json(['success'=> true, 'data' => $solar->getGeneration()]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'data' => 'Error: ' . $e->getMessage()]);
        }
    }

    //
    public function consumption(Request $request)
    {
        try {
            //$params = Solar::testParams();
            $params = $this->request->all();
            $solar = new Solar($params);
            $solar->computerConsumptionTable();

            return response()->json(['success'=> true, 'data' => $solar->getConsumption()]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'data' => 'Error: ' . $e->getMessage()]);
        }
    }

}
