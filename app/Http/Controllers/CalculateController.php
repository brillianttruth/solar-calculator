<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculateController extends Controller
{
    //
    public function info()
    {
        return view("info");
    }

    public function report(Request $request)
    {
        $data = $request->except('_token');
        return view("report", compact('data'));
    }

    public function inforeport(Request $request)
    {
        $data = $request->except('_token');
        return view("inforeport", compact('data'));
    }
}
