<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmailReportController extends Controller
{
    public function sendReportEmail(Request $request)
    {
        $url = $request->reportLink;
        $email = $request->toEmail;
        $data = [
            'url' => $url,
            'email' => $email
        ];
        
        $to      = $email;
        $subject = 'Report';
        $message = '<a href="'.$url.'">Go to Report</a>';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: solarshare.ie@example.com' . "\r\n" .
            "Cc: 6494684@forward.hubspot.com\r\n".
            'Reply-To: noreply@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        
        
        mail($to, $subject, $message, $headers);
        return redirect()->away($url);
    }
}
