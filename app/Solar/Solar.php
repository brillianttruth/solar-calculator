<?php


namespace App\Solar;

use Carbon\Carbon;
use App\Consumption;
use App\HourlyGeneration;
use App\HourlyConsumption;
use Exception;

class Solar
{
    const PROFILE_URBAN = 1;
    const PROFILE_RURAL = 2;

    private $lat;
    private $lng;
    private $peakPower;
    private $angel;
    private $aspect;
    private $estimatedConsumption;  //kWh
    private $profile;
    private $nightsaver;
    private $priceDay;
    private $priceNight;
    private $priceStandingCharge;

    private $generationTable = [];
    private $consumptionTable = [];

    public function __construct($params)
    {
        $this->lat            = $params['lat'] ?? null;
        $this->lng            = $params['lng'] ?? null;
        $this->peakPower      = $params['peakPower'] ?? 0;
        $this->angel          = $params['angel'] ?? 0;
        $this->aspect         = self::normalizeAspect($params['aspect'] ?? 0);
        $this->estimatedConsumption = $params['estimatedConsumption'] ?? 0;  //kWh
        $this->profile        = $params['profile'] ?? null;
        $this->nightsaver     = $params['nightsaver'] ?? null;
        $this->priceDay       = $params['priceDay'] ?? 0;
        $this->priceNight     = $params['priceNight'] ?? 0;
        $this->priceStandingCharge= $params['priceStandingCharge'] ?? 0;
    }

    //
    public static function normalizeAspect($aspect)
    {
        return $aspect%360;
    }

    //
    public static function testParams()
    {
        return [
            'lat'            => 53.4460306,
            'lng'            => -6.1449983,
            'peakPower'     => 4.02,
            'angel'          => 30,
            'aspect'         => 0,
            'dateStart'      => Carbon::now(),
            'dateEnd'        => Carbon::now()->addYear(1),
            'estimatedConsumption' => 5500,
            'profile'        => self::PROFILE_RURAL,
            'nightsaver'     => 0,
            'priceDay'       => 0.23,
            'priceNight'     => 0.10,
            'priceStandingCharge' => 167.24
        ];
    }

    //
    public function getLoadProfile()
    {
        if ($this->profile == self::PROFILE_URBAN && $this->nightsaver == false) {
            return 1;
        } elseif ($this->profile == self::PROFILE_URBAN && $this->nightsaver == true) {
            return 2;
        } elseif ($this->profile == self::PROFILE_RURAL && $this->nightsaver == false) {
            return 3;
        } elseif ($this->profile == self::PROFILE_RURAL && $this->nightsaver == true) {
            return 4;
        } else {
            throw new Exception('profile or nightsaver is not specified');
        }
    }

    //
    public function getGenerationTable()
    {
        return $this->generationTable;
    }

    //
    public function getGeneration()
    {
        return $this->generationTable;
    }

    //
    public function getConsumptionTable()
    {
        return $this->consumptionTable;
    }

    //
    public function getConsumption()
    {
        return $this->consumptionTable;
    }

    //
    private function getMonthlyGenerationTableQueryString()
    {
        $url = 'http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php';

        $params = http_build_query([
            'lat'           => $this->lat,
            'lon'           => $this->lng,
            'peakpower'     => $this->peakPower,
            'pvtechchoice'  => 'crystSi',
            'mountingplace' => 'building',
            'loss'          => 14,
            'angle'         => $this->angel,
            'aspect'        => $this->aspect,
            'outputformat'  => 'json'
        ]);

        return $url.'?'.$params;
    }

    /* returns array with detailed generation table:
    * [ month: number, kWh: number, hours: array ]
    */
    private function computerGenerationTableFromMonthlyData($monthlyData)
    {
        $this->generationTable = array_map(function($row) {
            $hourlyGenerations = HourlyGeneration::where('month', $row->month)->get();
            $hours = $hourlyGenerations->map(function($hourly) use($row) {
                    return [
                        'time' => $hourly->time,
                        'value' => $hourly->value,
                        'kWh' => $row->E_m * $hourly->value
                    ];
                })->sortBy('time');

            return [
                'month' => $row->month,
                'kWh' => $hours->sum('kWh'),
                'jrcKWh' => $row->E_m,
                'hours' => $hours->toArray()
            ];
        }, $monthlyData);
    }

    //
    public function computerGenerationTable()
    {
        $queryString = $this->getMonthlyGenerationTableQueryString();
        $content = json_decode(file_get_contents($queryString));

        $this->computerGenerationTableFromMonthlyData($content->outputs->monthly->fixed);
    }

    public function computerConsumptionTable()
    {
        $loadProfile = $this->getLoadProfile();
        $estimatedConsumption = $this->estimatedConsumption;
        $consumptions = Consumption::where('load_profile', $loadProfile)->orderBy('month')->get();

        $this->consumptionTable = array_map(function($consumption) use ($estimatedConsumption) {
            $hourlyConsumptions = HourlyConsumption::where('load_profile', $consumption['load_profile'])
                ->where('month', $consumption['month'])->get();
            $hours = $hourlyConsumptions->map(function($hourly) use($estimatedConsumption) {
                return [
                    'time' => $hourly->time,
                    'value' => $hourly->value,
                    'kWh' => $hourly->value * $estimatedConsumption
                ];
            })->sortBy('time');

            return [
                'month' => $consumption['month'],
                'lp' => $consumption['load_profile'],
                'consumptionDay' => $consumption['consumption_day'],
                'consumptionNight' => $consumption['consumption_night'],
                'kWhDay' => $consumption['consumption_day'] * $estimatedConsumption,
                'kWhNight' => $consumption['consumption_night'] * $estimatedConsumption,
                'kWh' => $hours->sum('kWh'),
                'hours' => $hours->toArray()
            ];
        }, $consumptions->toArray());
    }

    public function computerAll()
    {
        $this->computerGenerationTable();
        $this->computerConsumptionTable();
    }
}
