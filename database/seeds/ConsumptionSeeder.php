<?php

use Illuminate\Database\Seeder;

class ConsumptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $consumptions = [
            [1, 0.0738500, 0.0601100, 0.0761700, 0.0578200, 0.0219300, 0.0423400, 0.0210400, 0.0329600],
            [2, 0.0678300, 0.0574200, 0.0690500, 0.0590500, 0.0205600, 0.0405400, 0.0198500, 0.0343300],
            [3, 0.0668500, 0.0578600, 0.0691200, 0.0618300, 0.0203500, 0.0412500, 0.0216100, 0.0374500],
            [4, 0.0594500, 0.0508100, 0.0609900, 0.0553300, 0.0186300, 0.0340800, 0.0189000, 0.0326300],
            [5, 0.0577300, 0.0465400, 0.0576000, 0.0526800, 0.0186400, 0.0272100, 0.0186400, 0.0278900],
            [6, 0.0533700, 0.0426900, 0.0512000, 0.0488500, 0.0178400, 0.0223500, 0.0169400, 0.0245100],
            [7, 0.0549200, 0.0445700, 0.0551100, 0.0505100, 0.0182100, 0.0228600, 0.0178800, 0.0244500],
            [8, 0.0582600, 0.0448000, 0.0592600, 0.0504900, 0.0186400, 0.0227200, 0.0178000, 0.0249300],
            [9, 0.0586700, 0.0456400, 0.0561100, 0.0467500, 0.0186400, 0.0251900, 0.0173300, 0.0242800],
            [10, 0.0659000, 0.0501600, 0.0634500, 0.0506400, 0.0202600, 0.0297700, 0.0180800, 0.0272700],
            [11, 0.0699800, 0.0552800, 0.0693500, 0.0553800, 0.0207600, 0.0370700, 0.0191000, 0.0293500],
            [12, 0.0766200, 0.0586400, 0.0834900, 0.0588200, 0.0221500, 0.0402600, 0.0219800, 0.0319100]
        ];

        foreach($consumptions as $consumption) {
            // LP 1
            DB::table('consumptions')->insert([
                'load_profile' => 1,
                'month' => $consumption[0],
                'consumption_day' => $consumption[1],
                'consumption_night' => $consumption[5]
            ]);

            // LP 2
            DB::table('consumptions')->insert([
                'load_profile' => 2,
                'month' => $consumption[0],
                'consumption_day' => $consumption[2],
                'consumption_night' => $consumption[6]
            ]);

            // LP 3
            DB::table('consumptions')->insert([
                'load_profile' => 3,
                'month' => $consumption[0],
                'consumption_day' => $consumption[3],
                'consumption_night' => $consumption[7]
            ]);

            // LP 4
            DB::table('consumptions')->insert([
                'load_profile' => 4,
                'month' => $consumption[0],
                'consumption_day' => $consumption[4],
                'consumption_night' => $consumption[8]
            ]);

        }

    }
}
