<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConsumptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('consumptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('load_profile')->unsigned();
            $table->index('load_profile');
            $table->integer('month')->unsigned();
            $table->double('consumption_day')->unsigned();
            $table->double('consumption_night')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('consumptions');
    }
}
