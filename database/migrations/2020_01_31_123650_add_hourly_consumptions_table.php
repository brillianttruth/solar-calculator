<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHourlyConsumptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hourly_consumptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('load_profile')->unsigned();
            $table->index('load_profile');
            $table->integer('month')->unsigned();
            $table->integer('day')->unsigned();
            $table->integer('hour')->unsigned();
            $table->double('value')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hourly_consumptions');
    }
}
