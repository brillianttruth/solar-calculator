<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHourlyGenerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('hourly_generations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month')->unsigned();
            $table->integer('day')->unsigned();
            $table->integer('hour')->unsigned();
            $table->double('value')->unsigned();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('hourly_generations');
    }
}
