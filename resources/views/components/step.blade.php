<!--
<div class="form-top">
    <div class="card-block-head">
        <h4>{{ $num }} {{ $header }}</h4>
    </div>
</div>
-->

<div class="form-bottom">

    {{ $slot }}

    @if (isset($buttons) && in_array('previous', $buttons))
        <button type="button" class="btn btn-previous">Previous</button>
    @endif

    @if (isset($buttons) && in_array('next', $buttons))
        <button type="button" class="btn btn-next">Next</button>
    @endif

    @if (isset($buttons) && in_array('submit', $buttons))
        <button type="button" id="btn-report-link" class="btn">Show link</button>
    @endif
</div>
