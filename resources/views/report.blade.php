@extends('layouts.app')

@push('page-styles')

@endpush

@section('content')
<div class="inner-bg">
    <div class="container">
        
<!-- NEW -->

      <ul class="nav nav-tabs solar-tabs">
        <li><a data-toggle="tab" href="#tab0">Location</a></li>
        <li><a data-toggle="tab" href="#tab1">Generation</a></li>
        <li><a data-toggle="tab" href="#tab9">Demand</a></li>
        <li><a data-toggle="tab" href="#tab2">Storage</a></li>

        <li class="active"><a  data-toggle="tab" href="#tab3">Solar Survey Report</a></li>
        <li><a data-toggle="tab" href="#tab4">Finance</a></li>
        <li><a data-toggle="tab" href="#tab5">Book Survey</a></li>
      </ul>
      

      
      <div class="tab-content">
        <div id="tab0" class="tab-pane fade">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-block-head">
                        <h4>You have searched for location XXXX LOCATION XXXX</h4>
                    </div>
                </div>
            </div>
        </div>
         
        <div id="tab1" class="tab-pane fade">
          
        <form role="form" action="" class="report-form">
            @include('include.generation', $data)

        
          
        </div>
        <div id="tab2" class="tab-pane fade">
            
            @include('include.storage')

        </div>
        
                <div id="tab9" class="tab-pane fade">
          
           
           @include('include.consumption')

        </div>
        
        
        <div id="tab3" class="tab-pane fade in active">

            @include('include.balance')
            @include('include.booking')

        </div>
        
        <div id="tab4" class="tab-pane fade">
            
            @include('include.finance')
            
            
        </div>
        
        <div id="tab5" class="tab-pane fade">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card-block-head">
                        <h4>Book a survey</h4>
                    </div>
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            
                            
HUBSPOT
            

  
                        </div>
                    </div>
                </div>
            </div>


        </div>
        
        </form>
        
      </div>
      
<!-- END NEW -->
  

    </div>
</div>
<div hidden id="params" map-data="{{ $data['map-data'] }}"></div>
@endsection

@push('page-scripts')
    <script src="{{ asset('js/report.js') }}"></script>
@endpush
