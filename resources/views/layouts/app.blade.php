<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    
    <!-- Facebook -->
    
<meta property="og:url"                content="http://www.solarshare.ie" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="FREE Solar Calculator" />
<meta property="og:description"        content="How can you benefit from solar power?" />
<meta property="og:image"              content="http://dev.solarshare.ie/img/logo.png" />

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://use.typekit.net/dwd1srn.css">
    <link rel="stylesheet" href="{{ asset('css/form-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    @stack('page-styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('ico/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('ico/apple-touch-icon-57-precomposed.png') }}">
</head>
<body>

<!-- Top menu -->
@include('include.topmenu')

<!-- Top content -->
<div class="top-content">
    @yield('content')
</div>

<!-- Javascript -->
<script src="{{ asset('vendor/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/jquery.backstretch.min.js') }}"></script>
<script src="{{ asset('vendor/retina-1.1.0.min.js') }}"></script>
<script src="{{ asset('vendor/lodash-4.17.15.js') }}"></script>

<!-- App scripts -->
<script src="{{ asset('js/modules/apiClient.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/location.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/generation.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/consumption.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/powerStorage.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/balance.js?0.9.2') }}"></script>
<script src="{{ asset('js/modules/finance.js?0.9.2') }}"></script>
<script src="{{ asset('js/scripts.js?0.9.2') }}"></script>

@stack('page-scripts')

<!--[if lt IE 10]>
<script src="{{ asset('js/placeholder.js') }}"></script>
<![endif]-->

<div hidden id="map-key" data-key="{{ env('GOOGLE_MAP_KEY') }}"></div>
</body>

</html>
