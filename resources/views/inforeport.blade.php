@extends('layouts.app')

@push('page-styles')

@endpush

@section('content')
<div class="inner-bg">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <form role="form" action="" class="report-form">
                    <ul class="nav nav-tabs solar-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab0">Location</a></li>
                        <li><a data-toggle="tab" href="#tab1">Generation</a></li>
                        <li><a data-toggle="tab" href="#tab9">Demand</a></li>
                        <li><a data-toggle="tab" href="#tab2">Storage</a></li>
                        <li><a  data-toggle="tab" href="#tab3">Solar Survey Report</a></li>
                        <li><a data-toggle="tab" href="#tab4">VPP Economics</a></li>
                    </ul>
                      
                    <div class="tab-content solar-tabs-content">
                            <div id="tab0" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-block-head">
                                            @include('include.postcode', $data)
                                        </div>
                                    </div>
                                </div>
                            </div>         
                            <div id="tab1" class="tab-pane">
                                @include('include.generation', $data)
                            </div>
                            <div id="tab2" class="tab-pane">
                                @include('include.storage')
                            </div>
                            <div id="tab9" class="tab-pane">
                                @include('include.consumption')
                            </div>
                            <div id="tab3" class="tab-pane">
                                @include('include.balance')
                                @include('include.booking')
                            </div>
                            <div id="tab4" class="tab-pane">
                                @include('include.finance')
                            </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="button" class="btn btn-next">Next</button>
            </div>
        </div>
    </div>
</div>

<div hidden id="params" map-data="{{ $data['map-data'] ?? '{}' }}"></div>
@endsection

@push('page-scripts')
    <script src="{{ asset('js/inforeport.js') }}"></script>
@endpush
