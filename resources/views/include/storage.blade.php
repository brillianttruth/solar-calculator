<!-- HEADER -->
@include('include.section-header', ['header' => 'PowerStorage'])

<!-- CONTENT -->
<div class="row" id="storage">

    <!-- ELECTRICAL STORAGE -->
    <div class="col-lg-6">
        <div class="card h-100">
            <div class="card-header">Electrical storage</div>
            <div class="card-body">
                <div class="form-group">
                    <label for="batterySize">Battery size (kWh)</label>
                    <select class="form-control" id="batterySize">
                        <option value="0">0kWh</option>
                        <option value="2.5">2.5kWh</option>
                        <option value="5" selected>5kWh</option>
                        <option value="7.5">7.5kWh</option>
                        <option value="10">10kWh</option>
                        <option value="12.5">12.5kWh</option>
                        <option value="15">15kWh</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <!-- THERMAL STORAGE -->
    <div class="col-lg-6">
        <div class="card h-100">
            <div class="card-header">Thermal storage</div>
            <div class="card-body">
                <div class="form-check-inline">
                    <label class="checkbox-inline"><input type="checkbox" class="form-check-input" id="noImmersionTank" name="noImmersionTank">No immersion tank</label>
                </div>
                <div class="form-group">
                    <label for="immersionTankSize">Immersion tank size (Litres)</label>
                    <input type="number" name="immersionTankSize" value="120" class="form-control" id="immersionTankSize">
                </div>
            </div>
        </div>
    </div>

</div>
