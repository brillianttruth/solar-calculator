<!-- HEADER -->
@include('include.section-header', ['header' => 'Electricity Consumption'])

<!-- CONTENT -->
<div class="row" id="consumption">
    <div class="col-lg-4">
        <div class="card h-100">
            <div class="card-header">Electricity Today</div>
            <div class="card-body">

                <!-- ENERGY SUPPLIER -->
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Energy Supplier</label>
                    <select class="form-control" id="">
                        <option>Electric Ireland</option>
                        <option>Bord Gáis Energy</option>
                        <option>SSE Airtricity</option>
                        <option>Energia</option>
                        <option>PrePayPower</option>
                        <option>Panda</option>
                        <option>Pinergy</option>
                        <option>Other</option>
                    </select>
                </div>

                <!-- ELECTRICITY USAGE -->
                <div class="slidecontainer">
                    <label class="pull-left">3000kWh</label>
                    <label for="estimated-consumption">Estimated (<span id="estimated-consumption-caption">5500</span>kWh)</label>
                    <label class="pull-right">8000kWh</label>
                    <input type="range" min="3000" max="8000" step="100" value="5500" class="slider" id="estimated-consumption" name="estimated-consumption">
                </div>

                <!-- URBAN OR RURAL --> <!-- NIGHTSAVER -->
                <div class="form-check-inline">
                    <label class="radio-inline"><input type="radio" id="profile" name="profile" value="1" checked>Urban</label>
                    <label class="radio-inline"><input type="radio" id="profile" name="profile" value="2">Rural</label>
                    <label class="radio-inline checkbox-inline"><input type="checkbox" class="form-check-input" id="nightsaver" name="nightsaver">Nightsaver</label>
                </div>

                <!-- ELECTRICITY USAGE -->
                <div class="slidecontainer">
                    <label class="pull-left">0.14&euro;</label>
                    <label for="price-day">Unit Price (<span id="price-day-caption">0.23</span>&euro;)</label>
                    <label class="pull-right">0.30&euro;</label>
                    <input type="range" name="price-day" min="0.14" max="0.30" step="0.01" value="0.23" class="slider" id="price-day">
                </div>

                <div class="slidecontainer">
                    <label class="pull-left">0.04&euro;</label>
                    <label for="price-night">Night Rate (<span id="price-night-caption">0.10</span>&euro;)</label>
                    <label class="pull-right">0.12&euro;</label>
                    <input type="range" name="price-night" min="0.04" max="0.12"  step="0.01" value="0.10" class="slider" id="price-night">
                </div>

                <div class="slidecontainer">
                    <label class="pull-left">40&euro;</label>
                    <label for="price-standing-charge">Standing Charge (<span id="price-standing-charge-caption">167</span>&euro;)</label>
                    <label class="pull-right">60&euro;</label>
                    <input type="range" name="price-standing-charge" min="40" max="300" step="1" value="167" class="slider" id="price-standing-charge">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">MPRN</label>
                    <input type="email" class="form-control" id="mprn" name="mprn" placeholder="Enter MPRN">
                </div>

                <!-- GRID CARBON -->
                <div class="form-group">
                    <label for="grid-carbon">Grid Carbon</label>
                    <input type="text" class="form-control" value="437" id="grid-carbon" name="grid-carbon">
                </div>

                <p>Prices include VAT</p>
            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="card h-100">
            <div class="card-header">Electricity consumption</div>
            <div class="card-body" style="position:relative;">
                <div id="chart-consumption-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>
                <div id="chart-consumption-bg" class="chart-bg">
                    <span class="container-chart chart-bg-title">No data for these parameters</span>
                </div>
                <div id="chart-consumption" class="container-chart"></div>
                <hr>
                <h4 class="text-center">Annual Cost € <span id="annual-cost"></span></h4>

                <!-- CARBON EMISSIONS -->
                <h4 class="text-center">Carbon Emissions Tons <span id="carbon-emission"></span></h4>
            </div>
        </div>
    </div>
</div>
