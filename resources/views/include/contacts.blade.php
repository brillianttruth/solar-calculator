<h4>Your details</h4>
<div class="form-group">
    <input type="text" name="form-google-plus" placeholder="Email" class="form-google-plus form-control" id="form-google-plus">
</div>

<!-- NEW form-name-input -->
<div class="form-group">
    <input type="text" name="form-name-input" placeholder="Name" class="form-name-input form-control" id="form-name-input">
</div>

<!-- NEW form-phone-input -->
<div class="form-group">
    <input type="text" name="form-phone-input" placeholder="Phone" class="form-phone-input form-control" id="form-phone-input">
</div>

<!-- NEW Checkboxes-->
<div class="checkbox-outer">
    <div class="form-group">
        <div class="row">
            <div class="col-md-3">
                <label>Preferred communication</label>
            </div>
            <div class="col-md-9">
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="" name="">
                    Email
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="checkboxBuilding" name="checkboxBuilding">
                    Phone
                </label>
            </div>
        </div>
    </div>
</div>

<!-- NEW Checkboxes-->
<div class="checkbox-outer">
    <div class="form-group">
        <div class="row">
            <div class="col-md-3">
                <label>Preferred contact time</label>
            </div>
            <div class="col-md-9">
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="" name="">
                    Morning
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="checkboxBuilding" name="checkboxBuilding">
                    Midday
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="checkboxBuilding" name="checkboxBuilding">
                    Afternoon
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="checkboxBuilding" name="checkboxBuilding">
                    Evening
                </label>
            </div>
        </div>
    </div>
</div>

<!-- NEW Checkboxes-->
<div class="checkbox-outer">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
            SolarShare is committed to protecting and respecting your privacy, and we’ll only use your personal information to administer your account and to provide the products and services you requested from us. From time to time, we would like to contact you about our products and services, as well as other content that may be of interest to you. If you consent to us contacting you for this purpose, please tick below to say how you would like us to contact you:

    I agree to receive other communications from SolarShare.

You can unsubscribe from these communications at any time. For more information on how to unsubscribe, our privacy practices, and how we are committed to protecting and respecting your privacy, please review our Privacy Policy.

By clicking submit below, you consent to allow SolarShare to store and process the personal information submitted above to provide you the content requested.
</div>
            <div class="col-md-3">
                <label>I agree to regular marketing</label>
            </div>
            <div class="col-md-9">
                <label class="checkbox-inline">
                    <input type="checkbox" class="form-check-input" id="" name="">
                    Yes please
                </label>
            </div>
        </div>
    </div>
</div>


