<div class="form-group">
    
    <div class="card-block-head"><h4>Help us find your location. Please enter your postcode or address</h4></div>
    
    <div class="jumbotron jumbotron-fluid bg-dark" style="background-image: url(img/iemap.jpg); padding: 88px 22px; background-size: cover; background-repeat: no-repeat;">

    
    <input type="text" name="address" placeholder="Your address" class="form-control" id="address"
    	value="{{ $data['address'] ?? ''}}">
    </div>

</div>




