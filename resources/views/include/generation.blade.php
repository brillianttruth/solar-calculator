<!-- HEADER -->
@include('include.section-header', ['header' => 'Generation'])

<!-- CONTENT -->
<div class="row" id="generation">
    <div class="col-lg-4">
        <div class="card card-lg">
            <div class="card-header">Roof Calculations</div>
            <div class="card-body">

                <div class="form-group">
                    <label>Panel orientation</label>
                    <div class="">
                        <label class="radio-inline">
                            <input type="radio" id="orientation" name="orientation" value="0"
                                @if((isset($data['orientation']) && $data['orientation'] == 0) || !isset($data['orientation'])) checked @endif>Landscape
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="orientation" name="orientation" value="1"
                                @if(isset($data['orientation']) && $data['orientation'] == 1) checked @endif>Portrait
                        </label>
                    </div>
                </div>

                <!-- CONTROL FOR OVERLAY ROTATION -->
                <div class="slidecontainer">
                    <label class="pull-left">West</label>
                    <label for="heading">Orientate Solar Grid (<span id="heading-caption">{{ $data['heading'] ?? 0 }}&#176;</span>)</label>
                    <label class="pull-right">East</label>
                    <input type="range" min="-90" max="90" value="{{ $data['heading'] ?? 0 }}" class="slider" id="heading" name="heading">
                </div>      

                <!-- PANELS HORIZONTAL-->
                <div class="form-group">
                    <label for="rows">Panels (Rows)</label>
                    {{-- <select class="form-control" id="rows" name="rows">
                        @for ($i = 1; $i <= 7; $i++)
                        <option @if((isset($data) && $data['rows'] == $i) || (!isset($data) && $i == 3)) selected="selected" @endif>{{ $i }}</option>
                        @endfor
                    </select> --}}
                    <div class="row customContainer">
                        <input type="hidden" id="rows" name="rows" value="3" />
                        <div class="pull-left valueX">
                            <span class="rowsValue">3</span>
                        </div>
                        <div class="pull-right">
                            <button id="rowUp" type="button" class="customButton">+</button>
                            <button id="rowDown" type="button" class="customButton">-</button>
                        </div>
                    </div>
                </div>

                <!-- PANELS VERTICAL-->
                <div class="form-group">
                    <label for="cols">Panels (Columns)</label>
                    {{-- <select class="form-control" id="cols" name="cols">
                        @for ($i = 1; $i <= 7; $i++)
                        <option @if((isset($data) && $data['cols'] == $i) || (!isset($data) && $i == 4)) selected="selected" @endif>{{ $i }}</option>
                        @endfor
                    </select> --}}
                    <div class="row customContainer">
                        <input type="hidden" id="cols" name="cols" value="4" />
                        <div class="pull-left valueX">
                            <span class="colsValue">4</span>
                        </div>
                        <div class="pull-right">
                            <button id="colUp" type="button" class="customButton">+</button>
                            <button id="colDown" type="button" class="customButton">-</button>
                        </div>
                    </div>
                </div>

                <!-- ROOF SLOPE -->
                <div class="form-group slidecontainer">
                    <label class="pull-left">0&#176;</label>
                    <label for="pitch">Roof Slope (<span id="pitch-caption">{{ $data['pitch'] ?? 30 }}&#176;</span>)</label>
                    <label class="pull-right">90&#176;</label>
                    <input type="range" min="0" max="90" value="{{ $data['pitch'] ?? 30 }}" class="slider" id="pitch" name="pitch">
                </div>

                <div class="form-group">
                    <label>Shading</label>
                    <div>
                        <label class="checkbox-inline">
                            <input type="checkbox" class="form-check-input" id="checkboxTree" name="checkboxTree"
                                @if(isset($data['checkboxTree']) && $data['checkboxTree'] == 'on') checked @endif>Tree
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" class="form-check-input" id="checkboxBuilding" name="checkboxBuilding"
                                @if(isset($data['checkboxBuilding']) && $data['checkboxBuilding'] == 'on') checked @endif>Building
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" class="form-check-input" id="checkboxOther" name="checkboxOther"
                                @if(isset($data['checkboxOther']) && $data['checkboxOther'] == 'on') checked @endif>Other
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" class="form-check-input" id="checkboxChimney" name="checkboxChimney"
                                @if(isset($data['checkboxChimney']) && $data['checkboxChimney'] == 'on') checked @endif>Chimney
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" class="form-check-input" id="checkboxDontknowSh" name="checkboxShDontknowSh"
                                @if(isset($data['checkboxDontknow']) && $data['checkboxDontknow'] == 'on') checked @endif>Don't know
                        </label>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label>Roof type</label>
                        <div class="">
                            <label class="radio-inline">
                                <input type="radio" id="roofType" name="roofType" value="plain"
                                    @if((isset($data['roofType']) && $data['roofType'] == 'plain')) checked @endif>Plain tile
                            </label>
                            <label class="radio-inline">
                                <input type="radio" id="roofType" name="roofType" value="slate"
                                    @if((isset($data['roofType']) && $data['roofType'] == 'slate')) checked @endif>Slate
                            </label>
                            <label class="radio-inline">
                                <input type="radio" id="roofType" name="roofType" value="dontKnow"
                                    @if((isset($data['roofType']) && $data['roofType'] == 'dontKnow') || !isset($data['roofType'])) checked @endif>Don't know
                            </label>
                        </div>
                    </div>
                    
                    <hr>

                    <div class="form-group">
                        <label for="panelRating">Panel Rating (kW)</label>
                        <input class="form-control" id="panelRating" name="panelRating" value="335">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                System size (rows x columns)
                            </div>
                            <div class="col-sm-6">
                                <b><span id="system-size"></span> kW</b>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                Specific Yield
                            </div>
                            <div class="col-sm-6">
                                <b><span id="specific-yield"></span> kWh/kWp/year</b>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- MAP -->
    <div class="col-lg-8">
        <div class="card card-lg">
            <div class="form-group container-map">
                <div class="map-bg"> <span id="map-bg" class="container-map-2 map-bg-title">No data for this place</span> <span id="panorama-bg" class="container-map-2 map-bg-title">No panorama for this place</span> </div>
                <div id="map" ref="map" class="container-map-2" style="display: none"></div>
                <div id="panorama" class="container-map-2"></div>
            </div>
        </div>
    </div>

    <!-- CHART -->
    <div class="col-lg-8">
        <div class="card h-100">
            <div class="card-header">Forecast Electricity Generation</div>
            <div class="card-body" style="position:relative;">
                <div id="chart-generation-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>
                <div id="chart-generation-bg" class="chart-bg">
                    <span class="container-chart chart-bg-title">No data for these parameters</span>
                </div>
                <div id="chart-generation" class="container-chart"></div>
                <h3>Total generation <span id="total-generation"></span> kWh</h3>                
                <h4>Generation forecast produced using <a href="https://re.jrc.ec.europa.eu/pvg_tools/en/tools.html">PVGIS</a></h4>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="map-data" name="map-data" value={{ $data['map-data'] ?? ''}}>
