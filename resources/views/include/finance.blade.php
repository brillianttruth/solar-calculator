<div class="row">
  <div class="col-md-12">
    <div class="card-block-head">
      <h4>Solar Economics</h4>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">Cost chart</div>
      <div class="card-body" style="position:relative;">
        <div id="chart-cost-overlay" class="container-chart overlay"> <img src="{{ asset('img\loading.gif') }}" class="loader"> </div>
        <div id="chart-cost-bg" class="chart-bg"> <span class="container-chart chart-bg-title">No data for these parameters</span> </div>
        <div id="chart-cost" class="container-chart"></div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">Costs/benefits</div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-8">

            <div style="position:relative">
              <div id="chart-finance-overlay" class="container-chart overlay"> <img src="{{ asset('img\loading.gif') }}" class="loader"> </div>
              <div id="chart-finance-bg" class="chart-bg"> <span class="container-chart chart-bg-title">No data for these parameters</span> </div>
              <div id="chart-finance" class="container-chart"></div>
            </div>
			</div>
            <div class="col-md-4"> 
              
              <form id="finance-form">
                <!-- COST -->
                <div class="form-group">
                  <label for="cost">Cost</label>
                  <input type="text" class="form-control" value="12500" id="cost" name="cost">
                </div>

                <!-- GRANT -->
                <div class="form-group">
                  <label for="grant">Grant</label>
                  <input type="text" class="form-control" value="2800" id="grant" name="grant">
                </div>
                
                <!-- Upfront payment -->
                <div class="form-group">
                  <label for="upfront-payment">Upfront payment</label>
                  <input type="text" class="form-control" value="0" id="upfront-payment" name="upfront-payment">
                </div>              
  				
                <!-- TERM -->
                <div class="form-group">
                  <label for="term">Term</label>
                  <input type="text" class="form-control" value="7" id="term" name="term">
                </div>
  				
                <!-- RATE -->
                <div class="form-group">
                  <label for="rate">Rate %</label>
                  <input type="text" class="form-control" value="6.5" id="rate" name="rate">
                </div>

                <!-- GRID CARBON -->
                <!-- HIDE
                <div class="form-group">
                  <label for="grid-carbon">Grid Carbon</label>
                  <input type="text" class="form-control" value="437" id="grid-carbon" name="grid-carbon">
                </div>
                -->
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="col-md-12">
    <div class="card">
      <table class="table table-striped" id="finance-table"></table>
    </div>
  </div>
</div>
