<!-- HEADER -->
@include('include.section-header', ['header' => 'Solar Report'])

<!-- CONTENT -->

<div class="row">
    <!-- ILLUSTRATION -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body" style="position:relative;">
                <div id="schema-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>

                <div class="container-chart">
                    @include('include.svg-schema')
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BALANCE CHART -->
<div class="row" id="balance">
    
    
          <div class="col-lg-6">
    <div class="card h-100">
      <div class="card-header">Savings chart</div>
      <div class="card-body" style="position:relative;">
        <div id="chart-cost-overlay" class="container-chart overlay"> <img src="{{ asset('img\loading.gif') }}" class="loader"> </div>
        <div id="chart-cost-bg" class="chart-bg"> <span class="container-chart chart-bg-title">No data for these parameters</span> </div>
        <div id="chart-cost" class="container-chart"></div>
      </div>
    </div>
  </div>
  
  
      <div class="col-lg-6">
        <div class="card h-100">
            <div class="card-header">Carbon Emissions - tons of carbon dioxide</div>
            <div class="card-body" style="position:relative;">
                <div id="chart-co2-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>
                <div id="chart-co2-bg" class="chart-bg">
                    <span class="container-chart chart-bg-title">No data for these parameters</span>
                </div>
                <div id="chart-co2" class="container-chart"></div>
            </div>
        </div>
    </div>
  
  
    <div class="col-lg-6">
        <div class="card h-100">
            <div class="card-header">Usage chart</div>
            <div class="card-body" style="position:relative;">
                <div id="chart-usage-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>
                <div id="chart-usage-bg" class="chart-bg">
                    <span class="container-chart chart-bg-title">No data for these parameters</span>
                </div>
                <div id="chart-usage" class="container-chart"></div>
                <p>*requires installation of immersion diverter</p>
            </div>
        </div>
    </div>


    
    
        <div class="col-md-6">
        <div class="card h-100">
            <div class="card-header">Month usage chart</div>
            <div class="card-body" style="position:relative;">
                <div id="chart-monthly-usage-overlay" class="container-chart overlay">
                    <img src="{{ asset('img\loading.gif') }}" class="loader">
                </div>
                <div id="chart-monthly-usage-bg" class="chart-bg">
                    <span class="container-chart chart-bg-title">No data for these parameters</span>
                </div>
                <div id="chart-monthly-usage" class="container-chart"></div>
            </div>
        </div>
    </div>
    
    
</div>


