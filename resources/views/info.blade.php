@extends('layouts.app')

@section('content')

<div class="inner-bg">

  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 text">
        <h1>Free Solar Quote</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 form-box">
        <!--<div role="form" action="{{ route('report') }}" method="get" class="solar-form">-->
        <form role="form" action="{{ route('report') }}" method="get" class="solar-form">



        <fieldset data-step="1">
            
            <ul class="nav nav-tabs nav-justified solar-tabs">
                <li class="active"><a href="#">Location</a></li>
                <li><a href="#">Generation</a></li>
                <li><a href="#">Demand</a></li>  
                <li><a href="#">Storage</a></li>
                <li><a href="#">Contact Details</a></li>
                <li><a href="#">Solar Report</a></li>
                <li><a href="#">Finance</a></li>
                <li><a href="#">Book a survey</a></li>
            </ul>
  
  
            @component('components.step', ['num' => 1, 'header' => 'Location', 'buttons' => ['next']])
                @include('include.postcode')
            @endcomponent
        </fieldset>

        <fieldset data-step="2">
            
            <ul class="nav nav-tabs nav-justified solar-tabs">
                <li><a href="#">Location</a></li>
                <li class="active"><a href="#">Generation</a></li>
                <li><a href="#">Demand</a></li>  
                <li><a href="#">Storage</a></li>
                <li><a href="#">Contact Details</a></li>
                <li><a href="#">Solar Report</a></li>
                <li><a href="#">Finance</a></li>
                <li><a href="#">Book a survey</a></li>
            </ul>
            
          @component('components.step', ['num' => 2, 'header' => 'Generation', 'buttons' => ['previous', 'next']])
              @include('include.generation')
          @endcomponent
        </fieldset>
        
        <!-- ADD NEW STEP -->
        
        <fieldset data-step="3">
        
            <ul class="nav nav-tabs nav-justified solar-tabs">
                <li><a href="#">Location</a></li>
                <li><a href="#">Generation</a></li>
                <li class="active"><a href="#">Demand</a></li>  
                <li><a href="#">Storage</a></li>
                <li><a href="#">Contact Details</a></li>
                <li><a href="#">Solar Report</a></li>
                <li><a href="#">Finance</a></li>
                <li><a href="#">Book a survey</a></li>
            </ul>
        
		@component('components.step', ['num' => 3, 'header' => 'Demand', 'buttons' => ['previous', 'next']])
                @include('include.consumption')

                <!-- REPORT LINK -->
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-link" id="report-link"></a>
                    </div>
                </div>

            @endcomponent
        
        </fieldset>

        <fieldset data-step="4">
            
            <ul class="nav nav-tabs nav-justified solar-tabs">
                <li><a href="#">Location</a></li>
                <li><a href="#">Generation</a></li>
                <li><a href="#">Demand</a></li>  
                <li><a href="#">Storage</a></li>
                <li class="active"><a href="#">Contact Details</a></li>
                <li><a href="#">Solar Report</a></li>
                <li><a href="#">Finance</a></li>
                <li><a href="#">Book a survey</a></li>
            </ul>
            
            @component('components.step', ['num' => 4, 'header' => 'Contact details', 'buttons' => ['previous', 'submit']])
                @include('include.contacts')

                <!-- REPORT LINK -->
                <div class="row">
                    <div class="col-12">
                        <a class="btn btn-link" id="report-link"></a>
                    </div>
                </div>

            @endcomponent
            
        </fieldset>

        @csrf
      </form>
      <form id="reportLinkForm" action="{{ route('emailreport') }}" method="POST" style="display: none">
            @csrf
            <input type="hidden" value="dummy" id="reportLink" name="reportLink"/>
            <input type="hidden" value="test@test.com" id="toEmail" name="toEmail"/>
            <button class="btn" type="submit">Send Link</button>
        </form>
    </div>
  </div>

</div>

@endsection

@push('page-scripts')
    <script src="{{ asset('js/info.js') }}"></script>
@endpush
