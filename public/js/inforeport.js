jQuery(document).ready(function() {

    var location;
    var generation;
    var consumption = new Consumption;
    var storage = new PowerStorage;
    var balance;
    var finance;

    initMap();

    initCharts();

    addEventListeners();

    /* Init Map */
    function initMap() {
        window.mapCallback = function () {
            location = new Location({
                elem: '#address'
            });
            
            generation = new Generation({
                map: '#map',
                panorama: '#panorama'
            });

            location.bindMap(generation.getMap());

            balance = new Balance(generation, consumption, storage);
            finance = new Finance(balance);
        };

        const mapKey = $('#map-key').attr('data-key');
        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${mapKey}&callback=mapCallback&language=en&libraries=places,geometry`;
        document.body.appendChild(script);
    }

    function initCharts() {
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(function () {
            generation.initChart();
            consumption.initChart();
            balance.initCharts();
            finance.initCharts();
        });
    }

    function addEventListeners() {
        $('.btn-previous').on('click', function(e) {
            let $tab = $('.solar-tabs li.active');
            let $prevTab = $tab.prev();
            if ($prevTab) {
                $prevTab.find('a').tab('show');
            }
        });

        $('.btn-next').on('click', function(e) {
            let $tab = $('.solar-tabs li.active');
            let $nextTab = $tab.next();
            if ($nextTab) {
                $nextTab.find('a').tab('show');
            }            
        });

        $('#address').on('place_changed', function(e) {
            location.find()
                .done(function (data) {
                    generation.gotoLocation(data);
                });
        })
    }
});
