jQuery(document).ready(function() {

    var location;
    var generation;
    var consumption = new Consumption;

    initMap();

    initCharts();

    addEventListeners();

    /* Init Map */
    function initMap() {
        window.mapCallback = function () {
            location = new Location({
                elem: '#address'
            });

            generation = new Generation({
                map: '#map',
                panorama: '#panorama'
            });

            location.bindMap(generation.getMap());
        };

        const mapKey = $('#map-key').attr('data-key');
        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${mapKey}&callback=mapCallback&language=en&libraries=places,geometry`;
        document.body.appendChild(script);
    }

    function initCharts() {
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(function () {
            generation.initChart();
            consumption.initChart();
        });
    }

    function generateReportLink($form) {
        return document.location.origin + '/report/?' + $form.serialize();
    }

    function showReportLink($form) {
         var url = generateReportLink($form);
        // $('#report-link').attr('href', url);
        // $('#report-link').text("Report link");
        // $('#report-link').show();
        $("#reportLink").val(url);
        $("#reportLinkForm").show();
    }

    function hideReportLink() {
        $('#report-link').hide();
        $("#reportLinkForm").hide();
    }

    /* process step */
    function processStep(step) {
        if (step == 1) {
            location.find()
                .done(function (data) {
                    generation.gotoLocation(data, 1000);
                });
        } else if (step == 2) {
            hideReportLink();
        }
    }

    function addEventListeners() {
        $('.solar-form').on('step_changed', function(e, step) {
            processStep(step);
        });

        $('#btn-report-link').on('click', function() {
            var email = $("#form-google-plus").val();
            $("#toEmail").val(email);
            showReportLink($(this).parents('.solar-form'));
        });
    }

});
