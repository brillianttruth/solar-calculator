jQuery(document).ready(function() {

    var generation;
    var consumption = new Consumption;
    var storage = new PowerStorage;
    var balance;
    var finance;

    initMap();

    initCharts();

    /* Init Map */
    function initMap() {
        window.mapCallback = function () {
            generation = new Generation({
                map: '#map',
                panorama: '#panorama'
            });

            balance = new Balance(generation, consumption, storage);
            finance = new Finance(balance);
        };

        const mapKey = $('#map-key').attr('data-key');
        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${mapKey}&callback=mapCallback&language=en&libraries=places,geometry`;
        document.body.appendChild(script);
    }

    function initCharts() {
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(function () {
            generation.initChart();
            consumption.initChart();
            balance.initCharts();
            finance.initCharts();
        });
    }
});
