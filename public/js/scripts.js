jQuery(document).ready(function() {

    initBackstretch();

    addFormEventListeners();

    /* Init Fullscreen background */
    function initBackstretch() {
        $.backstretch(document.location.origin+"/img/backgrounds/1.jpg");

        $('#top-navbar-1').on('shown.bs.collapse', function () {
            $.backstretch("resize");
        });
        $('#top-navbar-1').on('hidden.bs.collapse', function () {
            $.backstretch("resize");
        });
    }

    /* Form event listeners*/
    function addFormEventListeners() {
        $('.solar-form fieldset:first-child').fadeIn('slow');

        $('.solar-form input[type="text"], .solar-form input[type="password"], .solar-form textarea').on('focus', function () {
            $(this).removeClass('input-error');
        });

        // next step
        $('.solar-form .btn-next').on('click', function () {
            var parent_fieldset = $(this).parents('fieldset');
            var next_step = true;

            parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function () {
                if ($(this).val() == "") {
                    $(this).addClass('input-error');
                    next_step = false;
                } else {
                    $(this).removeClass('input-error');
                }
            });

            if (next_step) {
                parent_fieldset.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    parent_fieldset.find('.solar-tabs').trigger('shown.bs.tab');
                });
            }

            $('.solar-form').trigger('step_changed', [parent_fieldset.attr('data-step')])

        });

        // previous step
        $('.solar-form .btn-previous').on('click', function () {
            var parent_fieldset = $(this).parents('fieldset');
            parent_fieldset.fadeOut(400, function () {
                $(this).prev().fadeIn();
                parent_fieldset.find('.solar-tabs').trigger('shown.bs.tab');
            });
        });

        // submit
        $('.solar-form').on('submit', function (e) {

            $(this).find('input[type="text"], input[type="password"], textarea').each(function () {
                if ($(this).val() == "") {
                    e.preventDefault();
                    $(this).addClass('input-error');
                } else {
                    $(this).removeClass('input-error');
                }
            });

        });
    }
});

