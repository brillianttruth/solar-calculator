function PowerStorage() {
    var batterySize = undefined;
    var immersionTankSize = undefined;
    var noImmersionTank = undefined;
    var availableCapacity = 0.9;

    /* constructor */

    setAttrsFromForm();

    addEventListeners();

    /* methods */

    function setAttrsFromForm() {
        batterySize = +$("#batterySize").val();
        immersionTankSize = +$("#immersionTankSize").val();
        noImmersionTank = $("#noImmersionTank").is(":checked") ? 1 : 0;
    }

    this.getAvailableCapacity = function () {
        return availableCapacity;
    };

    this.getBatterySize = function () {
        return batterySize;
    };

    function addEventListeners() {

        // attributes
        $("#batterySize, #noImmersionTank, #immersionTankSize").on("input", function() {
            setAttrsFromForm();

            if (noImmersionTank) {
                $('#immersionTankSize').parents('.form-group').hide();
            } else {
                $('#immersionTankSize').parents('.form-group').show();
            }

            $('#storage').trigger("storage_data_changed");
        });
    }
}
