function getApiData(url, params) {
    return $.ajax(url, {
        method: 'get',
        data: params,
        dataType: 'json',
        cache: false,
        timeout: 30000,
    })
        .pipe(function(data) {
            if (data.success) {
                return data.data;
            } else {
                return $.Deferred().reject();
            }
        });
}

function daysInMonth(month) {
    const date = new Date();
    return 32 - new Date(date.getFullYear(), month, 32).getDate();
}
