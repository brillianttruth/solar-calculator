function Balance(_generation, _consumption, _storage) {

    /* attributes */
    var self = this;

    var $usageChartOverlay = $('#chart-usage-overlay');
    var $usageChartBg = $('#chart-usage-bg');
    var $co2ChartOverlay = $('#chart-co2-overlay');
    var $co2ChartBg = $('#chart-co2-bg');
    var $costChartOverlay = $('#chart-cost-overlay');
    var $costChartBg = $('#chart-cost-bg');
    var $monthlyUsageChartOverlay = $('#chart-monthly-usage-overlay');
    var $monthlyUsageChartBg = $('#chart-month-usage-bg');
    var $schemaOverlay = $('#schema-overlay');

    var generation = _generation;
    var consumption = _consumption;
    var storage = _storage;

    var balanceData = null;
    var _usageChart = null;
    var _co2Chart = null;
    var _costChart = null;
    var _monthlyUsageChart = null;

    /* constructor */
    addEventListeners();

    function _computerBalanceData () {

        const generationData = generation.getGenerationData();
        const consumptionData = consumption.getConsumptionData();
        var batteryKWh = storage.getBatterySize();
        var currentBatteryKWh = storage.getBatterySize() * storage.getAvailableCapacity();

        if ( !generationData || !consumptionData) {
            balanceData = null;
            return;
        }

        var _data = [];

        // add generation data
        generationData.forEach(function(row) {
            if( !_data[row.month] ) {
                _data[row.month] = {
                    month: row.month,
                    hours: {}
                };
            }

            row.hours.forEach(function(hourRow) {
                if ( !_data[row.month].hours[hourRow.time] ) {
                    _data[row.month].hours[hourRow.time] = {};
                }

                _data[row.month].hours[hourRow.time].generationKWh = hourRow.kWh;
            })

            _data[row.month].generationKWh = row.kWh;
        });

        // add consumption data
        consumptionData.forEach(function(row) {
            if( !_data[row.month]) {
                _data[row.month] = {
                    month: row.month,
                    hours: {}
                };
            }

            row.hours.forEach(function(hourRow) {
                if ( !_data[row.month].hours[hourRow.time] ) {
                    _data[row.month].hours[hourRow.time] = {};
                }

                _data[row.month].hours[hourRow.time].consumptionKWh = hourRow.kWh;
            })

            _data[row.month].consumptionKWhDay = row.kWhDay;
            _data[row.month].consumptionKWhNight = row.kWhNight;
            _data[row.month].consumptionKWh = row.kWh;
            _data[row.month].consumptionEuroDay = row.euroDay;
            _data[row.month].consumptionEuroNight = row.euroNight;
            _data[row.month].consumptionEuroStandingCharge = row.euroStandingCharge;
            _data[row.month].consumptionEuroPso = row.euroPso;
            _data[row.month].consumptionEuro = row.euroDay + row.euroNight + row.euroStandingCharge + row.euroPso;

        });

        // compute some other values
        _data.forEach(function(row) {
            let monthUsedGridKWh = 0;
            let monthImmersionKWh = 0;

            for(time in row.hours) {
                let hourRow = row.hours[time];
                hourRow.startBatteryKWh = currentBatteryKWh;
                hourRow.netKWh = hourRow.consumptionKWh - hourRow.generationKWh;
                hourRow.endBatteryKWh = currentBatteryKWh - hourRow.netKWh;
                hourRow.usedGridKWh = Math.max(-hourRow.endBatteryKWh, 0);
                hourRow.waterKWh = Math.max(hourRow.endBatteryKWh - batteryKWh, 0);
                hourRow.immersionKWh = hourRow.waterKWh;

                currentBatteryKWh = hourRow.endBatteryKWh + hourRow.usedGridKWh - hourRow.immersionKWh;
                monthUsedGridKWh += hourRow.usedGridKWh;
                monthImmersionKWh += hourRow.immersionKWh;
            }

            row.usedGridKWh = monthUsedGridKWh;
            row.immersionKWh = monthImmersionKWh;
            row.usedPVKWh = row.generationKWh - row.immersionKWh;

            //row.usedPVKWh = Math.min(row.generationKWh + storage.getBatterySize()*5, row.consumptionKWhDay); // PV is used only during the day
            //row.immersionKWh = Math.max(row.generationKWh - storage.getBatterySize()*5 - row.usedPVKWh, 0);
            //row.usedGridKWh = Math.max(row.consumptionKWh - row.usedPVKWh, 0);
            //row.usedGridKWhDay = Math.max(row.consumptionKWhDay - row.usedPVKWh, 0);
            //row.usedGridKWhNight = row.consumptionKWhNight;
            //row.usedGridEuroDay = row.usedGridKWhDay * consumption.getPriceDay();
            //row.usedGridEuroNight = row.usedGridKWhNight * consumption.getPriceNight();
            //row.usedGridEuroStandingCharge = row.consumptionEuroStandingCharge;
            row.usedPVEuro = row.usedPVKWh * consumption.getPriceDay();
            row.usedGridEuro = row.consumptionEuro - row.usedPVEuro;
        });

        // add totals
        var total = {
            generationKWh: 0,
            consumptionKWhDay: 0,
            consumptionKWhNight: 0,
            consumptionKWh: 0,
            consumptionEuroDay: 0,
            consumptionEuroNight: 0,
            consumptionEuroStandingCharge: 0,
            consumptionEuroPso: 0,
            consumptionEuro: 0,
            usedPVKWh: 0,
            usedPVEuro: 0,
            //usedGridKWhDay: 0,
            //usedGridKWhNight: 0,
            usedGridKWh: 0,
            //usedGridEuroDay: 0,
            //usedGridEuroNight: 0,
            //usedGridEuroStandingCharge: 0,
            usedGridEuro: 0,
            immersionKWh: 0,
        };

        _data.forEach(function(row) {
            total.generationKWh += row.generationKWh;
            total.consumptionKWhDay += row.consumptionKWhDay;
            total.consumptionKWhNight += row.consumptionKWhNight;
            total.consumptionKWh += row.consumptionKWh;
            total.consumptionEuroDay += row.consumptionEuroDay;
            total.consumptionEuroNight += row.consumptionEuroNight;
            total.consumptionEuroStandingCharge += row.consumptionEuroStandingCharge;
            total.consumptionEuroPso += row.consumptionEuroPso;
            total.consumptionEuro += row.consumptionEuro
            total.usedPVKWh += row.usedPVKWh;
            total.usedPVEuro += row.usedPVEuro;
            //total.usedGridKWhDay += row.usedGridKWhDay;
            //total.usedGridKWhNight += row.usedGridKWhNight;
            total.usedGridKWh += row.usedGridKWh;
            //total.usedGridEuroDay += row.usedGridEuroDay;
            //total.usedGridEuroNight += row.usedGridEuroNight;
            //total.usedGridEuroStandingCharge += row.usedGridEuroStandingCharge;
            total.usedGridEuro += row.usedGridEuro;
            total.immersionKWh += row.immersionKWh;
        });

        balanceData = {
            rows: _data,
            total: total,
        };

        console.log('balanceData', balanceData);
        $('#balance').trigger('balance_data_changed', [balanceData]);
    }

    function drawUsageChart () {
        $usageChartBg.hide();
        $usageChartOverlay.hide();

        if ( !balanceData ) {
            setTimeout(function() {
                if ( !balanceData ) {
                    $usageChartBg.show();
                }
            }, 2000);
            return;
        }

        var _data = [];
        _data.push(['Index', 'Grid imported', 'PV used', 'To immersion*/grid']);
        _data.push(['Before', balanceData.total.consumptionKWh, 0, 0]);
        _data.push(['After', balanceData.total.usedGridKWh, balanceData.total.usedPVKWh, balanceData.total.immersionKWh]);


        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },
            hAxis: {
                title: 'kWh',
                titleTextStyle: {
                    color: '#333'
                }
            },
            vAxis: {
                minValue: 0
            },
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _usageChart.draw(data, options);
    }

    function drawCo2Chart () {
        $co2ChartBg.hide();
        $co2ChartOverlay.hide();

        if ( !balanceData ) {
            setTimeout(function() {
                if ( !balanceData ) {
                    $co2ChartBg.show();
                }
            }, 2000);
            return;
        }

        let carbonBefore = Math.round(consumption.getGridCarbonIntencity()*consumption.getEstimatedConsumption()/1000/10)/100;
        let carbonAfter = Math.round(consumption.getGridCarbonIntencity()*balanceData.total.usedGridKWh/1000/10)/100;

        var _data = [];
        _data.push(['Index', 'Carbon', 'Saving']);
        _data.push(['Before', carbonBefore, 0]);
        _data.push(['After', carbonAfter, carbonBefore - carbonAfter]);


        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },
            hAxis: {
                title: 'tons CO2e',
                titleTextStyle: {
                    color: '#333'
                }
            },
            vAxis: {
                minValue: 0
            },
            colors: ['#999999', '#eeeeee'],
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _co2Chart.draw(data, options);
    }

    function drawCostChart () {
        $costChartBg.hide();
        $costChartOverlay.hide();

        if ( !balanceData ) {
            setTimeout(function() {
                if ( !balanceData ) {
                    $co2ChartBg.show();
                }
            }, 2000);
            return;
        }

        var _data = [];
        _data.push(['Index', 'Cost', 'Saving']);
        _data.push(['Before', balanceData.total.consumptionEuro, 0]);
        _data.push(['After', balanceData.total.usedGridEuro, balanceData.total.consumptionEuro - balanceData.total.usedGridEuro]);


        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },
            hAxis: {
                title: '€',
                titleTextStyle: {
                    color: '#333'
                }
            },
            vAxis: {
                minValue: 0
            },
            colors: ['#45995a', '#d1eed7'],
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _costChart.draw(data, options);
    }

    function drawMonthlyUsageChart () {
        $monthlyUsageChartBg.hide();
        $monthlyUsageChartOverlay.hide();

        if ( !balanceData ) {
            setTimeout(function() {
                if ( !balanceData ) {
                    $monthlyUsageChartBg.show();
                }
            }, 2000);
            return;
        }

        var _data = [];
        _data.push(['Month', 'PV used', 'Grid imported', 'To immersion/grid']);

        balanceData.rows.forEach(function (row) {
            _data.push([row.month, row.usedPVKWh, row.usedGridKWh, row.immersionKWh]);
        });

        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },
            hAxis: {
                title: 'kWh',
                titleTextStyle: {
                    color: '#333'
                }
            },
            vAxis: {
                title: 'Months',
                titleTextStyle: {
                    color: '#333'
                },
                minValue: 0,
                maxValue: 12,
                gridlines: {
                    count: 0,
                },
                ticks: [2, 4, 6, 8, 10, 12],
            },
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _monthlyUsageChart.draw(data, options);
    }

    function drawSchema () {
        $schemaOverlay.hide();

        if ( !balanceData ) {
            return;
        }

        let peakPower = generation.getPeakPower();

        $('#svg-consumption-text').text(`${consumption.getEestimatedConsumption()}kWh`);
        //$('#svg-used-pv-text').text(`${Math.round((balanceData.total.usedPVKWh+balanceData.total.immersionKWh)*10)/10}kWh`);
        $('#svg-used-pv-text').text(`${Math.round((balanceData.total.generationKWh)*10)/10}kWh`);
        $('#svg-used-grid-text').text(`${Math.round((balanceData.total.usedGridKWh)/100)*100}kWh`);
        $('#svg-koef-text').text(`${Math.round(peakPower*10)/10}kW ${peakPower != 0 ? Math.round(balanceData.total.generationKWh/peakPower/10)*10 : 0}kWh/kWp/a`);
        $('#svg-battery-text').text(`${storage.getBatterySize()}kWh`);
        $('#svg-carbon-text').text(`${Math.round(consumption.getGridCarbonIntencity()*(balanceData.total.consumptionKWh - balanceData.total.usedGridKWh)/1000/1000*100)/100}tCO2e`);
        $('#svg-cost-text').text(`${Math.round(balanceData.total.consumptionEuro - balanceData.total.usedGridEuro)}€`);
    }

    function addEventListeners() {
        $('#generation').on('generation_data_changed', function() {
            _computerBalanceData();
            setAll();
        });

        $('#consumption').on('consumption_data_changed', function() {
            _computerBalanceData();
            setAll();
        });

        $('#storage').on('storage_data_changed', function() {
            _computerBalanceData();
            setAll();
        });

        // tabs (redraw charts)
        $('.solar-tabs').on("shown.bs.tab", function() {
            setAll();
        })
    }

    function setUsageChart() {
        $usageChartOverlay.show();
        drawUsageChart();
    }

    function setCo2Chart() {
        $co2ChartOverlay.show();
        drawCo2Chart();
    }

    function setCostChart() {
        $costChartOverlay.show();
        drawCostChart();
    }

    function setMonthlyUsageChart() {
        $monthlyUsageChartOverlay.show();
        drawMonthlyUsageChart();
    }

    function setSchema() {
        $schemaOverlay.show();
        drawSchema();
    }

    function setAll() {
        setUsageChart();
        setCo2Chart();
        setCostChart();
        setMonthlyUsageChart();
        setSchema();
    }

    function initUsageChart() {
        _usageChart = new google.visualization.BarChart(document.getElementById('chart-usage'));
        _co2Chart = new google.visualization.BarChart(document.getElementById('chart-co2'));
        _costChart = new google.visualization.BarChart(document.getElementById('chart-cost'));
        _monthlyUsageChart = new google.visualization.BarChart(document.getElementById('chart-monthly-usage'));
    };

    this.initCharts = function() {
        initUsageChart();
    }

    this.getBalanceData = function () {
        return balanceData;
    };

    this.getGeneration = function () {
        return generation;
    }

    this.getConsumption = function () {
        return consumption;
    }

}
