function Consumption() {
    /* attributes */
    var self = this;

    var $chartOverlay = $('#chart-consumption-overlay');
    var $chartBg = $('#chart-consumption-bg');

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var gridCarbonIntencity = 437;  // gCO2e / kWh
    var estimatedConsumption = undefined;
    var profile = undefined;
    var nightsaver = undefined;
    var priceDay = undefined;
    var priceNight = undefined;
    var priceStandingCharge = undefined;
    var pricePso = 38.68;

    var consumptionData = null;
    var _chart = null;

    /* constructor */

    setAttrsFromForm();

    addEventListeners();

    function setAttrsFromForm() {
        estimatedConsumption = +$("#estimated-consumption").val();
        profile = +$("#profile:checked").val();
        nightsaver = $("#nightsaver").is(":checked") ? 1 : 0;
        priceDay = +$("#price-day").val();
        priceNight = +$("#price-night").val();
        priceStandingCharge = +$("#price-standing-charge").val();
    }

    function _getPriceNight() {
        return nightsaver ? priceNight : priceDay;
    }

    this.getPriceDay = function () {
        return priceDay;
    };

    this.getPriceNight = function () {
        return _getPriceNight();
    };

    this.getEestimatedConsumption = function () {
        return estimatedConsumption;
    };

    this.getEstimatedConsumption = function () {
        return estimatedConsumption;
    };

    this.getGridCarbonIntencity = function () {
        return gridCarbonIntencity;
    };

    function getConsumptionData() {
        consumptionData = null;
        return getApiData('/api/consumption', {
            'profile': profile,
            'nightsaver': nightsaver,
            'estimatedConsumption': estimatedConsumption,
        }).pipe(function(data) {
            consumptionData = data;

            consumptionData.forEach(function(row) {
                row.euroDay = row.kWhDay * priceDay;
                row.euroNight = row.kWhNight * _getPriceNight();
                row.euroStandingCharge = priceStandingCharge / 365 * daysInMonth(row.month);
                row.euroPso = pricePso / 12;
            });

            console.log('cunsumptionData', consumptionData);
            $('#consumption').trigger('consumption_data_changed', [consumptionData]);

            return data;
        });
    }

    function drawChart () {
        $chartBg.hide();
        $chartOverlay.hide();

        var _data = [];
        _data.push(['Month', 'Day', 'Night']);
        if (consumptionData) {
            consumptionData.forEach(function (row) {
                _data.push([months[row.month-1], row.kWhDay, row.kWhNight]);
            });
        } else {
            $chartBg.show();
            return;
        }

        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },
            hAxis: {
                title: 'Months',
                titleTextStyle: {
                    color: '#333'
                }
            },
            vAxis: {
                title: 'kWh',
                minValue: 0
            },
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _chart.draw(data, options);
    }

    var _debouncedSetChart = _.debounce(function() {
        getConsumptionData()
            .always(function () {
                drawChart();
            });
    }, 200);

    function setChart() {
        $chartOverlay.show();
        _debouncedSetChart();
    };

    this.initChart = function () {
        _chart = new google.visualization.ColumnChart(document.getElementById('chart-consumption'));
        setChart();
    };

    this.getConsumptionData = function () {
        return consumptionData;
    };

    function addEventListeners () {
        // atributes
        $('#estimated-consumption, #profile, #nightsaver, #price-day, #price-night, #price-standing-charge').on("input", function() {
            setAttrsFromForm();
        });

        // charts
        $("#estimated-consumption, #profile, #nightsaver, #price-day, #price-night, #price-standing-charge").on("change", function() {
            setChart();
        });

        // sliders and others
        $("#estimated-consumption").on("input", function () {
            $('#estimated-consumption-caption').text(estimatedConsumption);
        });

        $("#price-day").on("input", function () {
            $('#price-day-caption').text(priceDay);
        });

        $("#price-night").on("input", function () {
            $('#price-night-caption').text(priceNight);
        });

        $("#price-standing-charge").on("input", function () {
            $('#price-standing-charge-caption').text(priceStandingCharge);
        });

        $("#consumption").on('consumption_data_changed', function() {
            let cost = consumptionData.reduce(function(sum, row) {
                return sum + row.euroDay + row.euroNight + row.euroStandingCharge;
            }, 0) + pricePso;

            let carbon = gridCarbonIntencity*estimatedConsumption/1000/1000;

            $('#annual-cost').text(Math.round(cost));
            $('#carbon-emission').text(Math.round(carbon*10)/10);
        });

        // tabs (redraw charts)
        $('.solar-tabs').on("shown.bs.tab", function() {
            drawChart();
        })
    }
}
