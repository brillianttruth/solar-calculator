    function Finance(_balance) {
    /* attributes */
    var self = this;

    var $financeChartOverlay = $('#chart-finance-overlay');
    var $financeChartBg = $('#chart-finance-bg');

    var costEuro = undefined;
    var grantEuro = undefined;
    var upfrontPayment = undefined;
    var loanEuro = undefined;
    var loanTerm = undefined;    // (years)
    var loanRate = undefined;    // (%)
    var gridCarbonIntencity = undefined;

    var reportTerm = 20;         // (years)
    var CO2Term = 30;            // (years)
    var inflationRate = 3;       // (%)
    

    var balance = _balance;

    var financeData = null;
    var _financeChart = null;
    var _financeTable = null;

    /* constructor */
    setAttrsFromForm();
    addEventListeners();

    function setAttrsFromForm () {
        costEuro = +$("#cost").val();
        grantEuro = +$("#grant").val();
        upfrontPayment = +$("#upfront-payment").val();
        loanEuro = Math.max(costEuro - grantEuro - upfrontPayment, 0);
        loanTerm = +$("#term").val();
        loanRate = +$("#rate").val();
        gridCarbonIntencity = +$("#grid-carbon").val();
    }

	function _computerFinanceData () {
        const balanceData = balance.getBalanceData();

        if ( !balanceData) {
            financeData = null;
            return;
        }

        var _data = [];

        let d = new Date();
        let year = d.getFullYear() + 1;
        let generationKWt = balance.getBalanceData().total.generationKWh;
        let degradationRate = balance.getGeneration().getDegradationRate();

        //let gridCarbonIntencity = balance.getConsumption().getGridCarbonIntencity();
        let priceDay = balance.getConsumption().getPriceDay();

        var loanMonthlyRate = loanRate / 100 / 12;
        var loanMonthlyEuro = 12 * loanEuro * (loanMonthlyRate + loanMonthlyRate / (Math.pow(1 + loanMonthlyRate, 12*loanTerm) - 1));
        var cumulativeEuro = 0;

        for (let y = 0; y < reportTerm; y++) {
            var curGridCarbonIntencity = loanTerm ? ( gridCarbonIntencity * (CO2Term-y-1) / CO2Term ) : gridCarbonIntencity;
            let gridCarbonTons = generationKWt * curGridCarbonIntencity / 1000;
            priceDay += priceDay * inflationRate / 100;
            let generationCostEuro = generationKWt * priceDay
            let loanRepaymentEuro = y<loanTerm ? -loanMonthlyEuro : 0;
            let netEuro = generationCostEuro + loanRepaymentEuro;
            cumulativeEuro += netEuro;

            _data.push({
                year: year,
                count: y+1,
                generationKWt: generationKWt,
                gridCarbonIntencity: curGridCarbonIntencity,
                gridCarbonTons: gridCarbonTons,
                generationPriceEuro: priceDay,
                generationCostEuro: generationCostEuro,
                loanRepaymentEuro: loanRepaymentEuro,
                netEuro: netEuro,
                cumulativeEuro: cumulativeEuro,
                monthlyCostSavingEuro: netEuro / 12,
            });

            year++;
            generationKWt -= generationKWt * degradationRate / 100;
        }

        financeData = _data;
	}

	function drawFinanceChart () {
        $financeChartBg.hide();
        $financeChartOverlay.hide();

        if ( !financeData ) {
            setTimeout(function() {
                if ( !financeData ) {
                    $financeChartBg.show();
                }
            }, 2000);
            return;
        }


        var _data = [];
        _data.push(['Year', 'Monthly costs/benefits', 'Cumulative costs/benefits']);
        if (financeData) {
            financeData.forEach(function(row) {
                _data.push([''+row.year, row.monthlyCostSavingEuro, row.cumulativeEuro]);
            });
        } else {
            $financeChartBg.show();
            return;
        }
        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            height: 500,
            legend: {
                position: "top"
            },
            series: {
                0: { targetAxisIndex: 0 },
                1: { targetAxisIndex: 1 }
            },
            hAxis: {
                title: 'Year',
                titleTextStyle: {
                    color: '#333'
                },
                slantedText: true,
                slantedTextAngle: 90,
                //ticks: financeData.map(row => row.year),
            },
            vAxes: {
                0: { title: 'Euro', gridlines: { count: 8 } },
                1: { title: 'Euro', gridlines: { count: 8 } },
            },
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            isStacked: true,
        };

        _financeChart.draw(data, options);
	}

    function setFinanceChart() {
        $financeChartOverlay.show();
        drawFinanceChart();
    }

    function initFinanceChart() {
        _financeChart = new google.visualization.ColumnChart(document.getElementById('chart-finance'));
    }

    function drawFinanceTable() {

        if ( !_financeTable ) {
            return;
        }

        var _data = [];
        if (financeData) {
            financeData.forEach(function(row) {
                _data.push([
                    row.year,
                    row.count,
                    row.generationCostEuro.toFixed(),
                    //row.gridCarbonIntencity.toFixed(1),
                    row.loanRepaymentEuro.toFixed(0),
                    row.netEuro.toFixed(0),
                    row.cumulativeEuro.toFixed(0),
                    row.monthlyCostSavingEuro.toFixed(0),
                ]);
            });
        } else {
            return;
        }

        _financeTable.clear();
        _financeTable.rows.add(_data).draw();
    }

    function setFinanceTable() {
        drawFinanceTable();
    }

    function setAll() {
        setFinanceChart();
        setFinanceTable();
    }

    function initFinanceTable() {

        _financeTable = $('#finance-table').DataTable({
            searching: false,
            ordering: false,
            bPaginate: false,
            //bLengthChange: false,
            info: false,
            pageLength: 20,
            //iDisplayLength: 20,
            columns: [
                { title: "Year" },
                { title: "Year count" },
                { title: "Cost saving (€)" },
                //{ title: "gCO2e" },
                { title: "Loan (€)" },
                { title: "Net (€)" },
                { title: "Cum (€)" },
                { title: "Saving (€)" },
            ],
        });
    }

    function addEventListeners() {
        // external data
        $('#balance').on('balance_data_changed', function() {
            _computerFinanceData();
            setAll();
        });

        // atributes
        $('#cost, #grant, #upfront-payment, #term, #rate, #grid-carbon').on("change", function() {
            setAttrsFromForm();
            _computerFinanceData();
            setAll();
        });
        
        // tabs (redraw charts)
        $('.solar-tabs').on("shown.bs.tab", function() {
            setAll();
        })
    }

	this.initCharts = function() {
        initFinanceChart();
        initFinanceTable();
    }

}