function Generation(params) {

    if (!$(params.map).length || !$(params.panorama).length) {
        return;
    }

    /* attributes */
    var self = this;
    var $chartOverlay = $('#chart-generation-overlay');
    var $chartBg = $('#chart-generation-bg');

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var map = {
        $elem: $(params.map),
        $elemBg: $(params.map+'-bg'),
        key: $(params.map).attr('data-key'),
        center: undefined,
        _map: null,
        _maxZoomService: null,
    };

    var panorama = {
        $elem: $(params.panorama),
        $elemBg: $(params.panorama+'-bg'),
        position: undefined,
        pov: undefined,
        _panorama: null
    };

    var panels = {
        center: undefined,
        rows: undefined,
        cols: undefined,
        heading: undefined,
        pitch: undefined,
        orientation: undefined,
        panelRating: 335,   // (W)
        panelWidth: 1.7,
        panelHeight: 1,
        marginTop: 0.2,
        marginRight: 0.5,
        marginBottom: 0.5,
        marginLeft: 0.5,
        coords: [],
        _polygon: null
    };

    var degradationRate = 0.6;  // (%)
    var generationData = null;
    var _chart = null;

    /* constructor */

    setAttrsFromForm();

    addEventListeners();

    initMap();

    /* methods */

    function _setMapDataInputs () {

        let mapCenter = map._map.getCenter();
        let panoramaPosition;
        let panoramaPov;
        let panelsCenter;

        if (panorama._panorama) {
            panoramaPosition = panorama._panorama.getPosition();
            panoramaPov = panorama._panorama.getPov();
        }

        // panels
        if (panels.center) {
            let coords = panels._polygon.getPath().getArray();
            let topLeftPoint = coords[0];

            var panelHeight = _getProjectionHeight(panels.panelHeight);
            var panelWidth = _getProjectionWidth(panels.panelWidth);

            let heading = 360 - panels.heading;

            let topPoint = google.maps.geometry.spherical.computeOffset(topLeftPoint, panelWidth * panels.cols / 2, 90 + heading);

            let centerPoint = google.maps.geometry.spherical.computeOffset(topPoint, panelHeight * panels.rows / 2, 180 + heading);
            panelsCenter = { lat: centerPoint.lat(), lng: centerPoint.lng() };
        } else {
            panelsCenter = null;
        }

        $("#map-data").val(JSON.stringify({
            'map-center': mapCenter,
            'panorama-position': panoramaPosition,
            'panorama-pov': panoramaPov,
            'panels-center': panelsCenter
        }));

        let mapData = $('#map-data').val();
        mapData = mapData ? JSON.parse(mapData) : [];
    }

    function setAttrsFromForm() {
        let mapData = $('#map-data').val();
        mapData = mapData ? JSON.parse(mapData) : [];

        map.center = mapData['map-center'] || { lat: 52.475192, lng: -1.884653 };
        panorama.position = mapData['panorama-position'];
        panorama.pov = mapData['panorama-pov'] || { heading: 34, pitch: 10 };

        panels.center = mapData['panels-center'];
        panels.rows = +$("#rows").val();
        panels.cols = +$("#cols").val();
        panels.heading = +$("#heading").val();
        panels.pitch = +$("#pitch").val();
        panels.orientation = +$("#orientation:checked").val();
        panels.panelRating = +$("#panelRating").val();
        _switchPanelsWidthHeight();
    }

    function _switchPanelsWidthHeight () {
        let width = panels.panelWidth;
        let height = panels.panelHeight;

        // horizontal
        if (panels.orientation == 0) {
            panels.panelWidth = Math.max(width, height);
            panels.panelHeight = Math.min(width, height);

            // vertical
        } else {
            panels.panelWidth = Math.min(width, height);
            panels.panelHeight = Math.max(width, height);
        }
    }

    function _getProjectionWidth (width) {
        return width;
    }

    function _getProjectionHeight (height) {
        return height * Math.cos(panels.pitch * Math.PI / 180);
    }

    function _getPeakPower () {
        return panels.rows * panels.cols * panels.panelRating / 1000;
    }

    this.getPeakPower = function () {
        return _getPeakPower();
    };

    function _getPanelsCoords () {
        var coords = [];
        if ( !panels.center ) { return coords; }

        var panelHeight = _getProjectionHeight(panels.panelHeight);
        var panelWidth = _getProjectionWidth(panels.panelWidth);
        var heading = 360-panels.heading;

        // set offset of pivot point of panels
        //let verticalOffsetPivot = -panelHeight * panels.rows / 2 - panels.marginBottom;
        //let horizontalOffsetPivot = 0;

        // get top left point for all panels
        let centerPointPanels = new google.maps.LatLng(panels.center.lat, panels.center.lng);

        let topPointPanels = google.maps.geometry.spherical.computeOffset(centerPointPanels, panelHeight * panels.rows / 2, heading);

        let topLeftPointPanels = google.maps.geometry.spherical.computeOffset(topPointPanels, panelWidth * panels.cols / 2, 270 + heading);

        for (let i = 0; i < panels.rows; i++) {
            for (let j = 0; j < panels.cols; j++) {

                // get top left point for current panel
                let topPoint = google.maps.geometry.spherical.computeOffset(topLeftPointPanels, panelHeight * i, 180 + heading);
                let topLeftPoint = google.maps.geometry.spherical.computeOffset(topPoint, panelWidth * j, 90 + heading);

                // get all points for current panel
                let point1 = topLeftPoint;
                let point2 = google.maps.geometry.spherical.computeOffset(point1, panelWidth, 90 + heading);
                let point3 = google.maps.geometry.spherical.computeOffset(point2, panelHeight, 180 + heading);
                let point4 = google.maps.geometry.spherical.computeOffset(point3, panelWidth, 270 + heading);

                // set coords
                coords[i*panels.cols + j] = [
                    { lat: point1.lat(), lng: point1.lng() },
                    { lat: point2.lat(), lng: point2.lng() },
                    { lat: point3.lat(), lng: point3.lng() },
                    { lat: point4.lat(), lng: point4.lng() },
                ];
            }
        }

        return coords;
    };

    // extra area rectangle
    function _withExtraAreaCoords (coords) {
        if (coords.length == 0) { return }

        let point1 = new google.maps.LatLng(coords[0][0]);
        let point2 = new google.maps.LatLng(coords[panels.cols - 1][1]);
        let point3 = new google.maps.LatLng(coords[panels.cols * panels.rows - 1][2]);
        let point4 = new google.maps.LatLng(coords[panels.cols * (panels.rows - 1)][3]);

        var marginTop = _getProjectionHeight(panels.marginTop);
        var marginRight = _getProjectionWidth(panels.marginRight);
        var marginBottom = _getProjectionHeight(panels.marginBottom);
        var marginLeft = _getProjectionWidth(panels.marginLeft);

        var heading = 360-panels.heading;
        // add margins

        point1 = google.maps.geometry.spherical.computeOffset(point1, marginTop, heading);
        point1 = google.maps.geometry.spherical.computeOffset(point1, marginLeft, 270 + heading);

        point2 = google.maps.geometry.spherical.computeOffset(point2, marginTop, heading);
        point2 = google.maps.geometry.spherical.computeOffset(point2, marginRight, 90 + heading);

        point3 = google.maps.geometry.spherical.computeOffset(point3, marginBottom, 180 + heading);
        point3 = google.maps.geometry.spherical.computeOffset(point3, marginRight, 90 + heading);

        point4 = google.maps.geometry.spherical.computeOffset(point4, marginBottom, 180 + heading);
        point4 = google.maps.geometry.spherical.computeOffset(point4, marginLeft, 270 + heading);

        coords[panels.cols * panels.rows] = [
            { lat: point1.lat(), lng: point1.lng() },
            { lat: point2.lat(), lng: point2.lng() },
            { lat: point3.lat(), lng: point3.lng() },
            { lat: point4.lat(), lng: point4.lng() },
        ];

        return coords;
    }

    function drawPanels() {
        if (panels._polygon) {
            panels._polygon.setMap(null);
        }

        if ( !panels.center ) { return }

        var panelsCoords = _getPanelsCoords();
        panelsCoords = _withExtraAreaCoords(panelsCoords);

        panels._polygon = new google.maps.Polygon({
            paths: panelsCoords,
            strokeColor: "white",
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
            draggable: true
        });

        panels._polygon.setMap(map._map);

        // add panels events listeners
        panels._polygon.addListener('dragend', function() {
            _setMapDataInputs();
            setAttrsFromForm();
        })
    };

    function _setMap (location) {
        map.$elem.show();
        map.center = location;
        panels.center = map.center;
        map._map.setCenter(map.center);

        initPanels();
        setChart();
    }

    function _hidePanorama () {
        panorama._panorama.setVisible(false);
        panorama.$elem.hide();
        panorama.$elemBg.show();
    }

    function _setPanorama (location, pov) {

        if ( !location ) {
            _hidePanorama();
            return;
        }

        var _sv = new google.maps.StreetViewService();
        _sv.getPanorama({
            location: location,
            radius: 50,
        }, function(data, status) {
            if (status === 'OK') {
                _setMapDataInputs();
                panorama.$elemBg.hide();
                panorama.$elem.show();
                panorama._panorama.setPano(data.location.pano);
                if (pov) {
                    panorama._panorama.setPov(pov);
                } else {
                    panorama._panorama.setPov({
                        heading: google.maps.geometry.spherical.computeHeading(
                            data.location.latLng,
                            new google.maps.LatLng(panels.center.lat, panels.center.lng)),
                        pitch: 0
                    })
                }
                panorama._panorama.setVisible(true);
            } else {
                _setMapDataInputs();
                panorama.$elem.hide();
                panorama.$elemBg.show();
            }
        });
    }

    function _gotoLocationWithPanorama (location, panoramaPosition, panoramaPov, delay) {
        _setMap(location);

        _hidePanorama();
        setTimeout(function() {
            _setPanorama(panoramaPosition, panoramaPov);
        }, delay)
    };

    this.gotoLocation = function(location, delay=0) {
        _gotoLocationWithPanorama(location, location, null, delay);
    };

    this.getMap = function() {
        return map._map;
    };

    function drawChart () {
        $chartBg.hide();
        $chartOverlay.hide();

        if ( !_chart ) {
            return;
        }

        var _data = [];
        _data.push(['Month', 'Generation']);
        if (generationData) {
            generationData.forEach(function(row) {
                _data.push([months[row.month-1], row.jrcKWh]);
            });
        } else {
            $chartBg.show();
            return;
        }

        var data = google.visualization.arrayToDataTable(_data);

        var options = {
            legend: {
                position: "top"
            },            
            hAxis: {
                title: 'Months',
                titleTextStyle: {
                    color: '#333'
                },
                gridlines: {
                    count: 1
                },
            },
            vAxis: {
                title: 'kWh',
                minValue: 0
            },
            animation: {
                startup: true,
                duration: 1000,
                easing: 'inOut',
            },
            curveType: 'function',
        };

        _chart.draw(data, options);
    }

    function getGenerationData() {
        generationData  = null;

        if ( !panels.center ) {
            return $.Deferred().reject();
        }

        return getApiData('/api/generation', {
            'lat'           : panels.center.lat,
            'lng'           : panels.center.lng,
            'peakPower'     : _getPeakPower(),
            'mountingPlace' : '',
            'loss'          : 0,
            'angel'         : panels.pitch,
            'aspect'        : -panels.heading,
        }).pipe(function(data) {
            generationData = data;
            console.log('generationData', generationData);
            $('#generation').trigger('generation_data_changed', [generationData]);
            return data;
        });
    }

    var _debouncedSetChart = _.debounce(function() {
        getGenerationData()
            .always(function () {
                drawChart();
            });
    }, 200);

    function setChart() {
        $chartOverlay.show();
        _debouncedSetChart();
    };

    function setLabels() {

        // computer and draw text fields
        let systemSize = panels.rows*panels.cols*panels.panelRating/1000;
        var totalGeneration = generationData ? generationData.reduce(function(sum, row) {
                return sum + row.jrcKWh;
            }, 0) : 0;
        let specificYield = systemSize!=0 ? totalGeneration/systemSize : 0;

        $("#system-size").text(Math.round(systemSize*100)/100);
        $("#specific-yield").text(Math.round(specificYield*10)/10);

        $('#total-generation').text(Math.round(totalGeneration*10)/10);
    }

    function initMap() {

        // map
        map._map = new google.maps.Map(map.$elem.get(0), {
            center: map.center,
            zoom: 16.5,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            disableDefaultUI: false,
            zoomControl: true,
            tilt: 0,
        });

        // max available zoom
        map._maxZoomService = new google.maps.MaxZoomService();
        map._maxZoomService.getMaxZoomAtLatLng(map.center, function(maxZoomResult) {
            if (google.maps.MaxZoomStatus.OK) {
                map._map.setZoom(Math.min(maxZoomResult.zoom, 21));
            }
        });

        map._map.addListener('dragend', function() {
            _setMapDataInputs();
            setAttrsFromForm();
        });

        // panorama
        panorama._panorama = new google.maps.StreetViewPanorama(panorama.$elem.get(0));
        _setPanorama (panorama.position, panorama.pov);

        panorama._panorama.addListener('pov_changed', function() {
            _setMapDataInputs();
            setAttrsFromForm();
        });
        map._map.setStreetView(panorama._panorama);

        initPanels();

        _setMapDataInputs();
    }

    function initPanels() {
        drawPanels();
    }

    this.initChart = function() {
        _chart = new google.visualization.ColumnChart(document.getElementById('chart-generation'));
        setChart();
    }

    this.getGenerationData = function () {
        return generationData;
    }

    this.getDegradationRate = function () {
        return degradationRate;
    }

    function addEventListeners() {

        // atributes
        $("#rows, #cols, #heading, #pitch, #orientation, #panelRating").on("input", function() {
            setAttrsFromForm();
        });

        // panels
        $("#rows, #cols, #heading, #pitch, #orientation, #panelRating").on("input", function() {
            drawPanels();
            setLabels();
        });

        // chart
        $("#rows, #cols, #heading, #pitch, #panelRating").on("change", function() {
            setChart();
        });

        // sliders and others
        $("#heading").on("input", function() {
            $('#heading-caption').text((-panels.heading) + '°');
        });

        $("#pitch").on("input", function() {
            $('#pitch-caption').text(panels.pitch + '°');
        });
        
        // Custom JS
        $.fn.triggerMultiple = function(list) {
            return this.each(function(){
                var $this = $(this); // cache target
        
                $.each(list.split(" "), function(k, v){ // split string and loop through params
                    $this.trigger(v); // trigger each passed param
                });
            });
        };
        
        $("#rowUp").on('click', (e) => {
           var curRow = $("#rows").val();
           curRow = parseInt(curRow)
           if(curRow < 7){
               curRow++;
               $("#rows").val(curRow)
               $(".rowsValue").html(curRow)
               $("#rows").triggerMultiple('input change')
           }
        });
        $("#rowDown").on('click', (e) => {
           var curRow = $("#rows").val();
           curRow = parseInt(curRow)
           if(curRow > 1){
               curRow--;
               $("#rows").val(curRow)
               $(".rowsValue").html(curRow)
               $("#rows").triggerMultiple('input change')
           }
        });
        
        $("#colUp").on('click', (e) => {
           var curCol = $("#cols").val();
           curCol = parseInt(curCol)
           if(curCol < 7){
               curCol++;
               $("#cols").val(curCol)
               $(".colsValue").html(curCol)
               $("#cols").triggerMultiple('input change')
           }
        });

        $("#colDown").on('click', (e) => {
           var curCol = $("#cols").val();
           curCol = parseInt(curCol)
           if(curCol > 1){
               curCol--;
               $("#cols").val(curCol)
               $(".colsValue").html(curCol)
               $("#cols").triggerMultiple('input change')
           }
        });

        $("#generation").on('generation_data_changed', function() {
            setLabels();
        });

        // tabs (redraw charts)
        $('.solar-tabs').on("shown.bs.tab", function() {
            google.maps.event.trigger(panorama._panorama, 'resize');
            drawChart();
        })        
    }
    
}
