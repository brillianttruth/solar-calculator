function Location (params) {

    if (!$(params.elem).length) {
        return;
    }

    /* attributes */
    var self = this;

    var point = undefined;

    var $elem = $(params.elem);
    var _autocomplete = null;
    var _searchService = null;    

    /* methods */

    this.find = function() {

        var dfd = $.Deferred();
        var request = {
            query: $elem.val(),
            fields: ['name', 'geometry'],
        };

        point = null;

        _searchService.findPlaceFromQuery(request, function(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                point = results[0].geometry.location;
                dfd.resolve({ lat: point.lat(), lng: point.lng() });
            }  else {
                dfd.reject();
            }
        });

        return dfd;
    };

    this.bindMap = function(__map) {
        _map = __map;

        // search
        _searchService = new google.maps.places.PlacesService(_map);

        // autocomplete
        _autocomplete = new google.maps.places.Autocomplete($elem.get(0));
        _autocomplete.addListener('place_changed', function() {
            $elem.trigger('place_changed');
        });

        this.checkValue();
    }

    this.checkValue = function() {
        if ($elem.val()) {
            $elem.trigger('place_changed');
        }
    }
}
